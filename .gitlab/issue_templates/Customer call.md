Title: Customer call - [Fill in company name / domain]

#### Overview: 

+ Customer: 
+ Ticket: (if applicable)
+ Timezone: 
+ Requested dates / times: 

NOTE: if call is intended to occur less than 48 hrs from now, review individual's Google Calendars for likely fit and `@` mention them in a comment on this issue.

#### Timezones/Schedule:

{{INSERTIMAGE}}

1. Visit http://www.timeanddate.com/worldclock/meeting.html?year=2020&month=01&day=01&p1=224&p2=125&p3=94&p4=44&p5=56&p6=132&p7=236
2. Click "Add more cities"
3. Add the customers timezone/city
4. Take a screenshot of the customers working hours in relation to the support team. 
5. Paste the screenshot under **Timezones/Schedule:**
6. Determine support team members that may be available for the call - based on timezones. Ping them in this issue.
7. Assign to support ([support lead](https://about.gitlab.com/jobs/support-lead/)

//cc @ernstvn 


(Mark issue as confidential) 